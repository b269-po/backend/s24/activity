const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const fullAddress = ["258 Washington Ave NW", "California", "90011"]
const [street, city, zipCode] = fullAddress;
console.log(`I live at ${street}, ${city} ${zipCode}`);

const animal = {
	type: "Saltwater Crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in.",
};

const {type, weight, measurement} = animal
console.log(`Lolong was a ${type}. He weighed at ${weight} with a measurement of ${measurement}`);

const numbers = [1, 2, 3, 4, 5];
	numbers.forEach((number) => {
	console.log(number);
})

const reduceNumbers = [1, 2, 3, 4, 5];
	let sum = reduceNumbers.reduce((accumulator,curValue) => accumulator + curValue, 0)
	console.log(sum);



	

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog();
myDog.name = "Zeus",
myDog.age = 3,
myDog.breed = "Beagle"
console.log(myDog);

const myDog1 = new Dog("Friankie", 5, "Miniature Dachshund");
console.log(myDog1);

